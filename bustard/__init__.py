#!/usr/bin/env python
# -*- coding: utf-8 -*-

__version__ = '0.0.1'
__title__ = 'bustard'
__author__ = 'mozillazg'
__license__ = 'MIT'
__copyright__ = 'Copyright (c) 2015 mozillazg'
